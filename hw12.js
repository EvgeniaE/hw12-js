// Почему для работы с input не рекомендуется использовать события клавиатуры?
// На современных устройствах есть и другие способы «ввести что-то».
// Например, распознавание речи (это особенно актуально на мобильных устройствах) или
// Копировать/Вставить с помощью мыши.
// Поэтому для того, чтобы корректно отслеживать ввод в поле <input>, то одних клавиатурных
//  событий недостаточно.


const elem = document.querySelectorAll(".btn");
elem.forEach((letter) => {
  document.addEventListener("keydown", function (event) {
    const code = event.code;
    const attr = letter.dataset.letter;
    console.log(code);
    console.log(attr);
    if (code === attr || code === `ShiftLeft${attr}`  ||  code === `shifrRight${attr}` ) {
      document.querySelector(`[data-letter=${attr}]`)
        .classList.remove("passive");

      document.querySelector(`[data-letter=${attr}]`).classList.add("active");
    } else if(code === `Numpad${attr}`)    {
      document.querySelector(`[data-letter=${attr}]`)
      .classList.remove("passive");

    document.querySelector(`[data-letter=${attr}]`).classList.add("active");
    }else {
      document
        .querySelector(`[data-letter=${attr}]`)
        .classList.remove("active");
      document.querySelector(`[data-letter=${attr}]`).classList.add("passive");
    }
  });
});
